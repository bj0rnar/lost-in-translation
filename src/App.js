import Profile from './Components/Profile/Profile'
import Login from './Components/Login/Login'
import Translator from './Components/Translation/Translator'
import {BrowserRouter, Switch} from 'react-router-dom'
import PublicRoute from "./Utility/PublicRoute";
import PrivateRoute from "./Utility/PrivateRoute";
import './App.css';

/**
 * TODO: LocalStorage get isLoggedIn
 * TODO: Route guard, isLoggedIn=false -> Login, otherwise go to translate
 */

function App() {
    return (
        <BrowserRouter>
            <Switch>
                <PublicRoute path='/' exact component={Login}/>
                <PrivateRoute path="/profile" component={Profile}/>
                <PrivateRoute path="/translate" component={Translator}/>
            </Switch>
        </BrowserRouter>
    );
}

export default App;
