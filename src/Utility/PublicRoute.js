import React from 'react';
import { Route, Redirect } from "react-router-dom";

const PublicRoute = props => {

    const user = localStorage.getItem('user');

    //If a user is already logged in, send them directly to the translated section
    //This way, login is only available for logged out users.
    if (user !== null){
        return <Redirect to="/translate"/>
    }

    return <Route {...props} />
}

export default PublicRoute