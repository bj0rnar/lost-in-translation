import React from 'react';
import { Route, Redirect } from "react-router-dom";

const PrivateRoute = props => {


    const user = localStorage.getItem('user')

    //If the current user is not logged in, redirect them to the login page

    if (user === null) {
        return <Redirect to="/"/>
    }

    return <Route {...props} />
}

export default PrivateRoute

