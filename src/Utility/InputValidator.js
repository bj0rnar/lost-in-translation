function validateInput(input) {
    //Don't allow input, anything but a-z letters and spaces and a length below fourty characters.
    return input.trim() !== '' && (/^[a-zA-Z ]+$/).test(input) && input.length <= 40;
}

export default validateInput;