import TranslateImages from "./TranslateImages";
import {useState} from 'react'
import styles from './Translator.module.css'
import {Link} from "react-router-dom";
import logo from "../../Assets/icon-with-splash.png";
import isPermittedInput from "../../Utility/InputValidator";

export function Translator() {

    //A state for keeping track of stuff written in input field
    const [text, setText] = useState('');

    //A state for transferring text to child component
    const [translateArray, setTranslateArray] = useState([])

    const translateButton = () => {

        //input validation on state
        if (isPermittedInput(text)) {

            //Extract current user object and the translation array
            const currentUser = JSON.parse(localStorage.getItem('user'));
            const translations = currentUser.translations;

            //Use pop and unshift to effectively maintain a LIFO stack
            if (translations.length >= 10) {
                translations.pop();
                translations.unshift(text)
            } else {
                translations.unshift(text)
            }

            //Parse and update user with new translations array
            localStorage.setItem('user', JSON.stringify(currentUser))

            //Push all text to translate array state, which is passed to child components through props
            let arr = []
            for (let i = 0; i < text.length; i++)
                //if there's an empty string we push a custom empty image
                text.charAt(i) !== " " ? arr.push(text.charAt(i).toLowerCase()) : arr.push('space')
            setTranslateArray(arr)
        }
    }

    const getTextFromInput = e => {
        //Sets state to the first 40 characters of string in input field
        setText(e.target.value.substr(0, 40))
    }

    return (
        <div className={styles.translateWrapper}>
            <div className={styles.headerWrapper}>
                <div className={styles.logoContainer}>
                    <img className={styles.logoImage} src={logo} alt="logo"/>
                    <p>Lost in Translation!</p>
                </div>
                <div>
                    <Link to="/profile">
                        <button className={styles.profileButton} type="button"><span className="material-icons" id={styles.iconButton}>account_circle</span></button>
                    </Link>
                </div>
            </div>
            <main>
                <div className={styles.inputWrapper}>
                    <input className={styles.inputField} type="text" onChange={getTextFromInput}
                           placeholder="Translate away.."/>
                    <button className={styles.inputButton} type="button" onClick={translateButton}><span
                        className='material-icons' id={styles.material_icon}>arrow_forward</span></button>
                </div>

                <div className={styles.translateContainer}>
                    <div className={styles.imageWrapper}>
                        <TranslateImages content={translateArray}/>
                    </div>
                    <footer className={styles.imageFooter}>
                        <p>Translation</p>
                    </footer>
                </div>

            </main>

        </div>
    )
}

export default Translator