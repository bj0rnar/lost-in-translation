import styles from './Translator.module.css'

function TranslateImages({content}){

    //Retrieve content from parents translateArray state and map it with Image()

    return (
        <div className='translate-items'>
            <section>
                { content.map((content, index) => <img className={styles.signImages} key={index} src={require(`../../Assets/individial_signs/${content}.png`).default} alt="sign"/>) }
            </section>
        </div>
    )
}

export default TranslateImages