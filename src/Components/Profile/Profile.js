import styles from './Profile.module.css'
import ProfileItem from './ProfileItem'
import {Link } from "react-router-dom";
import { useState } from 'react';
import logo from "../../Assets/icon-with-splash.png";

export function Profile() {

    console.log('profile has been loaded')

    const currentUser = JSON.parse(localStorage.getItem('user'));

    const content = currentUser.translations

    const [array] = useState(content);


    const onButtonClicked = () => {
        localStorage.clear()
        console.log('localstorage has been cleared')
    }

    return (
        <div className={styles.profileWrapper}>
            <div className={styles.headerContainer}>
                <Link to="/translate"><button className={styles.backButton} type="button"> <span className="material-icons" id={styles.icon}>arrow_back</span> </button></Link>

                <div className={styles.logoContainer}>
                    <img src={logo} alt="logo"/>
                    <h3 className={styles.bannerText}>Profile</h3>
                </div>
            </div>

            <div className={styles.profileNameContainer}>
                <h1 className={styles.profileNameText}>{currentUser.username}</h1>
            </div>

            <div>
                <div className={styles.translationContainer}>
                    <div className={styles.itemContainer}>
                        <h1 className={styles.itemContainerHeaderText}>Last 10 translations</h1>
                        { array.map((content, index) => <ProfileItem key={index} content={content}/>) }
                    </div>
                    <footer className={styles.itemContainerFooter}>
                        <br/>
                    </footer>
                </div>

                <Link to='/'><button className={styles.returnButton} type="button" onClick={ onButtonClicked }>Logout <span className="material-icons">cancel</span></button></Link>

            </div>
            </div>
    )
}

export default Profile