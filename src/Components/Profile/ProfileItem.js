import styles from './ProfileItem.module.css'

function ProfileItem({content}){
    return (
        <div className={styles.profileItem}>
            <section>
                { content }
            </section>
        </div>
    )
}

export default ProfileItem