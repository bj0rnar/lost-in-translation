import { useState } from "react";
import { Link } from "react-router-dom";
import styles from './Login.module.css'

function LoginForm() {

    //user name state
    const [ username, setUsername ] = useState('')

    //set user name on input typed in input field
    const onInputChange = event => {
        setUsername(event.target.value)
    }

    const onInputClick = () => {

        //backup if the guard fails.
        if (localStorage.getItem('user') === null){
            //creates a new user
            const userObject = {
                username: username,
                translations: ['']
            }
            //store user
            localStorage.setItem('user', JSON.stringify(userObject))
        }
    }
    return (
        <form>
            <div className={styles.inputContainer}>
                <div>
                    <input className={styles.inputField} onChange={ onInputChange } type="text" placeholder="  What's your name?"/>
                    <Link to="/translate">
                        <button id={styles.arrowButton} className="material-icons" onClick={ onInputClick } type="button">arrow_forward</button>
                    </Link>
                </div>
            </div>
        </form>
    )
}

export default LoginForm