import LoginForm from "./LoginForm";
import styles from './Login.module.css'
import logo from "../../Assets/icon-hello-with-splash.png";

function Login() {

    return (
        <div className={styles.container}>
            <div className={styles.bannerContainer}>
                <h3 className={styles.bannerText}>Lost in Translation</h3>
            </div>
            <div className={styles.headerContainer}>
                <img src={logo} alt="lost-in-translation"/>
                <div className={styles.headerTextContainer}>
                    <h1 className={styles.headerH1}>Lost in Translation</h1>
                    <h2 className={styles.headerH2}>Get started</h2>
                </div>
            </div>
            <div className={styles.formContainer}>
                <LoginForm />
            </div>
            <footer className={styles.footerContainer}>

            </footer>
        </div>
    )
}

export default Login